<?php

use Poundation\PMailAddress;

use Poundation\PDictionary;

/** @Document (repositoryClass="Epr_User_Collection") */
class Epr_User extends Epr_Document
{
	const DOCUMENTNAME = 'Epr_User';
		
	/** @Id */
	private $id;
	
    /**
     * @Field(type="string")
     */
    private $lastname;

    /** @Field(type="string") */
    private $firstname;

    /**
     * @Index
     * @Field(type="string") */
    private $email;

    /**
     * @index
     * @Field(type="string") */
    private $password;

    /**
     * @Index
     * @Field(type="string") */
    private $role;

    /** 
     * @Field(type="boolean") */
    private $active;

    /** @var bool */
    private $hasAuth;

    /**
     * @var string
     * @Field(type="string")
     */
    private $salt;


    public function __construct($email) {
    	if (__($email)->length() > 0) {
    		$this->setEmail($email);
    	} else {
    		throw new Exception('A user can only be instanciated with an email address.');
    	}
    }


    /**
     * Returns the ID of the document.
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
	 * Sets the last name of the user.
	 * @param string $name
	 */
    public function setLastname($lastname){
        $this->lastname = $lastname;
    }
    
    /**
     * Returns the last name of the user.
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
    /**
     * Sets the firstname of the user.
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
    	$this->firstname = $firstname;
    }
    
    /**
     * Returns the firstname of the user.
     * @return string
     */
    public function getFirstname()
    {
    	return $this->firstname;
    }

    /**
     * Sets the mail address of the user.
     * @param Poundation\PMailAddress $email
     */
    public function setEmail($email)
    {
    	if (!$email instanceof PMailAddress) {
    		$email = PMailAddress::createFromString($email);
    	}
        $this->email = $email;
    }

	/**
	 * Returns the mail address of the user.
	 * @return \Poundation\PMailAddress
	 */
    public function getEmail()
    {
        return $this->email;
    }


    public function setPassword($password)
    {
    	$globalSalt = false;
    	if (Zend_Registry::isRegistered('salt')) {
    		$globalSalt = Zend_Registry::get('salt');
    	} else {
    		throw new Exception('No global salt found. How on hell did you bypass the bootstrapper?');
    	}
    	
        $this->generateSalt();
        $userSalt = $this->getSalt();

        $this->password =  md5($globalSalt . $password . $userSalt);
    }

	public function checkPassword($input) {
		
		$globalSalt = false;
		$userSalt = $this->getSalt();
		
		if (Zend_Registry::isRegistered('salt')) {
			 $globalSalt = Zend_Registry::get('salt');
		} else {
			throw new Exception('No global salt found. How on hell did you bypass the bootstrapper?');
		}
		
		$hash = md5($globalSalt . $input . $userSalt);
		return ($this->password === $hash);
	}

    public function getSalt()
    {
        return $this->salt;
    }

    public function generateSalt()
    {
        $this->salt = mt_rand();
    }
    
    /**
     * Returns the role identifier.
     * @return string
     */
    public function getRole(){
        return $this->role;
    }
    
    /**
     * Sets the role identifier if it is known.
     * @param string $role
     * @throws Exception
     */
    public function setRole($role)
    {
        _logger()->debugPrint(Epr_Roles::allRoles());
    	if (Epr_Roles::allRoles()->allKeys()->contains($role)) {

    		$this->role = $role;
    	} else {
    		throw new Exception('Unknown user role ' . $role . '.');
    	}

    }
    
    public function isAdministrator() {
    	return ($this->getRole() === Epr_Roles::ROLE_ADMIN);
    }
    
    public function setAsAuthenticated($hasAuth)
    {
        $this->hasAuth = $hasAuth;
    }

    public function hasAuth()
    {
        return $this->hasAuth;
    }

    /**
     * Deactivates the account.
     */
    public function deactivate() {
    	$this->setActive(false);
    }
    
    /**
     * Activates the account. 
     */
    public function activate() {
    	$this->setActive(true);
    }
    
    public function setActive($active)
    {
        $this->active = $active;
    }
    
    /**
     * Returns true of the user is active.
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

}
