<?php

interface Epr_Module {
	
	/* Implent these methods */
	static public function identifier();
	static public function title();
	static public function description();

	static public function instance();

	public function getSettingsFormFields();
	
	/**
	 * @param Zend_Form_Element $field
	 */
	public function saveSettingsField($field);
	
	/* These methods are implemented by Epr_Module_Abstract */
	public function getId();
	public function isActive();
	
	public function getAPIPath();	// return false or empty string to disable API functionality
	
	public function getAPIAuthLevel();			// implemented by abstract, override only if you know what you're doing
	public function setAPIAuthLevel($role);
	
	public function getDisplayname();
	public function setDisplayname($displayname);
}
