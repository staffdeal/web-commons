<?php
use Poundation\PArray;

/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_User_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository
{
	
	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository(Epr_User::DOCUMENTNAME);
	}
	
    static function getAllGuests()
    {
		throw new Exception('deprecated call of getAllGuests');
    }

    static function getByRole($role)
    {
		if (Epr_Roles::allRoles()->allKeys()->contains($role)) {
			$roleUsers = self::getRepository()->findBy(array('role' => $role));
			return parray($roleUsers);
		} else {
			throw new Exception('Querying unknown user role ' . $role . '.');
		}
    }
    
    static function hasActiveAdministrator() {
    	$repo = self::getRepository();
    	$activeAdmins = parray($repo->findBy(array('role' => Epr_Roles::ROLE_ADMIN)));
    	return ($activeAdmins->count() > 0);
    }

    static function getAllUsers()
    {
		$repo = self::getRepository();
		$users = $repo->findAll();
		return parray($users);
    }
}