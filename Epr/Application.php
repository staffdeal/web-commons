<?php

use Poundation\PURL;

use Poundation\PDictionary;

use Poundation\PClass;

use Poundation\PArray;

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;


/** @Document */
class Epr_Application extends Epr_Document {

	const VERSION=1;
	
	const RUNTIMETYPE_DEVELOPMENT = 'development';
	const RUNTIMETYPE_TESTING = 'testing';
	const RUNTIMETYPE_PRODUCTION = 'production';
	
	const DOCUMENTNAME='Epr_Application';
	const ID='application';
	
	/** @Id(strategy="ASSIGNED") */
	private $Id=Epr_Application::ID;
	
	/** @Field(type="integer") */
	private $documentsVersion = 0;
	
	private static $instance;
		
	private $moduleClasses = false;
	
	/** @EmbedMany */
	private $modules = array();
	
	/**
	 * Returns the singleton application object.
	 * @return Epr_Application
	 */
	static function application() {
		
		if (!isset(self::$instance)) {
			$dm = Zend_Registry::get('dm');
			self::$instance = $dm->find(Epr_Application::DOCUMENTNAME, Epr_Application::ID);
			if (!isset(self::$instance)) {
				self::$instance = new Epr_Application();
				$dm->persist(self::$instance);
				$dm->flush(self::$instance);
			}
			self::$instance->performUpdateActionsIfNeeded();
		}
		return self::$instance;
	}
	
	public function __destruct() {
		_dm()->flush();
	}
	
	public function isProduction() {
		return (APPLICATION_ENV == self::RUNTIMETYPE_PRODUCTION);
	}
	
	public function isDevelopment() {
		return (APPLICATION_ENV == self::RUNTIMETYPE_DEVELOPMENT);
	}
	
	public function isTesting() {
		return (APPLICATION_ENV == self::RUNTIMETYPE_TESTING);
	}
	
	public function getPublicURL() {
		$config = Zend_Registry::get('dispatcher');
		if ($config) {
			$urlString = $config->web->url;		// no need to check this further since existance is already ensured at bootstraping
			return new PURL($urlString);
		}
	}
	
	private function performUpdateActionsIfNeeded() {
		if ($this->getDocumentsVersion() < $this->getVersion() || $this->isDevelopment()) {
			$this->performUpdateActions();	
		}
	}
	
	private function performUpdateActions() {
		$this->registerDBViews();
		$this->registerModules();
	}
		
	private function registerDBViews() {
		$couchClient = _dm()->getCouchDBClient();
		
		$auth = new FolderDesignDocument(APPLICATION_PATH . "/library/couch/auth");
		$couchClient->createDesignDocument("auth", $auth);
		
		$ideas = new FolderDesignDocument(APPLICATION_PATH . "/library/couch/ideas");
		$couchClient->createDesignDocument("ideas", $ideas);
		
		$news = new FolderDesignDocument(APPLICATION_PATH . "/library/couch/news");
		$couchClient->createDesignDocument("news", $news);
		
		$this->documentsVersion = $this->getVersion();
	}
	
	public function getVersion() {
		return Epr_Application::VERSION;
	}	
		
	public function getDocumentsVersion() {
		return $this->documentsVersion;
	}
	
	public function getModules() {
		return $this->modules;
	}
	
	/**
	 * Returns all modules that expose an API.
	 * @return array
	 */
	public function getAPIModules() {
		$modules = array();
		foreach($this->getModules() as $module) {
			if ($module instanceof Epr_Module_Builtin_Abstract) {
				$apiPath = $module->getAPIPath();
				if ($apiPath !== false && $module->getAPIAuthLevel() != Epr_Roles::ROLE_NONE) {
					$modules[] = $module;
				}
			}
		}
		return $modules;
	}
	
	public function getUserRegistrationModules() {
		$modules = array();
		foreach($this->getModules() as $module) {
			if (PClass::classFromObject($module)->implementsInterface('Epr_Module_UserRegistration')) {
				$modules[] = $module;
			}
		}
		return $modules;
	}
	
	/**
	 * Returns class objects (no instances) of all known modules.
	 * @return \Poundation\PArray
	 */
	private function getModulesClasses() {
		
		if ($this->moduleClasses === false) {
		
			// first, we grap every file in the modules directory
			$path = ROOT_PATH . '/application/library/Epr/Modules';
			$files = scandir($path);
		
			$sourceFiles = new PArray();
			if (is_array($files)) {
				foreach($files as $filename) {
					$filename = __($filename);
					if ($filename->hasSuffix('.php')) {
						$sourceFiles->add($filename);
					}
				}
			}
		
			// then we create PClass objects with every file/class.
			$this->moduleClasses = new PArray();
			foreach($sourceFiles as $sourceFilename) {
				$fullPath = $path . '/' . $sourceFilename;
				include_once($fullPath);
				$className = 'Epr_Modules_' . $sourceFilename->removeTrailingCharactersWhenMatching('.php');
				
				$class = PClass::classFromString($className);
				if ($class->implementsInterface('Epr_Module') && $class->isKindOfClass('Epr_Module_Abstract')) {
					$this->moduleClasses->add($class);
				}
			}
		}
		return $this->moduleClasses;
	}
	
	private function hasModule($module) {
		$result = false;
		foreach($this->modules as $existingModule) {
			if ($existingModule == $module) {
				$result = true;
				break;
			}
		}
		return false;
	}
	
	public function getModuleWithIdentifier($id) {
		foreach($this->modules as $module) {
			if (PClass::classFromObject($module)->implementsInterface('Epr_Module')) {
                $moduleIdentifier = $module->identifier();
				if ($moduleIdentifier == $id || $moduleIdentifier == 'Modules_' . $id) {
					return $module;
				}
			}
		}
		return false;
	}
		
	private function registerModules() {
		$needFlush = false;
		foreach($this->getModulesClasses() as $class) {
			if ($class instanceof PClass) {
				
				$identifier = $class->invokeMethod('identifier');
				if ($identifier && $this->getModuleWithIdentifier($identifier) === false) {
					$instance = $class->invokeMethod('instance', array());
					$this->modules[] = $instance;
					$needFlush = true;					
				}				
			}
		}
		if ($needFlush) {
			_dm()->flush();
		}
	}
}

?>