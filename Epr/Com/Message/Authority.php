<?php

interface Epr_Com_Message_Authority {

    /**
     * Returns the key that is used to sign a message.
     * @return string
     */
    public function signingKey();

    /**
     * Returns the key the communication partner is supposed to use.
     * @return string
     */
    public function remoteKey();



}