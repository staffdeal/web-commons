<?php

class Epr_Com_Message {

	protected $name;
	
	protected $values = array();

    private $key = false;

    private $authority;
	
	static public function createMessageWithName($name) {
		if (is_string($name) && strlen($name) > 0) {
			$newMessage = new Epr_Com_Message();
			$newMessage->name = $name;
			return $newMessage;
		}
		
		return NULL;
	}
	
	static public function createMessageFromXML($xml) {
		
		if (is_string($xml)) {
			
			$document = new DOMDocument('1.0', 'UTF-8');
			if ($document->loadXML($xml)) {

				$nameElements = $document->getElementsByTagName('name');
				if ($nameElements->length > 0) {
					$nameElement = $nameElements->item(0);
					$name = $nameElement->nodeValue;
					if (strlen($name) > 0) {
						$message = self::createMessageWithName($name);
						if ($message) {

                            $keyElements = $document->getElementsByTagName('key');
                            if ($keyElements->length > 0) {
                                $keyElement = $keyElements->item(0);
                                $key = $keyElement->nodeValue;
                                if (strlen($key) > 8) {
                                    $message->key = $key;
                                }
                            }

                            $valuesContainerNodes = $document->getElementsByTagName('values');
							if ($valuesContainerNodes->length > 0) {
								$valuesContainerNode = $valuesContainerNodes->item(0);
								
								$containerChildNodes = $valuesContainerNode->childNodes;
								for ($i = 0; $i < $containerChildNodes->length; $i++) {
									
									$value = false;
									$key = false;
									
									$childNode = $containerChildNodes->item($i);
									if ($childNode->nodeName === 'value') {
										$value = $childNode->nodeValue;
										$attributes = $childNode->attributes;
										for ($u = 0; $u < $attributes->length; $u++) {
											$attribute = $attributes->item($u);
											if ($attribute->nodeName === 'key') {
												$key = $attribute->nodeValue;
											}
										}
									}
									
									if ($value !== false && $key !== false) {
										$message->values[$key] = $value;
									}
								}
							}

							return $message;
						}
					}
				}
				
			}
		}
		
		return NULL;
	}

    /**
     * Returns the message name.
     * @return string
     */
    public function getName() {
		return $this->name;
	}

    /**
     * Returns the value of a message parameter.
     * @param string $key
     * @return string|bool
     */
    public function getValue($key) {
		return (isset($this->values[$key]) ? $this->values[$key] : false);
	}

    /**
     * Returns all message parameters.
     * @return array
     */
    public function getValues() {
		return $this->values;
	}

    /**
     * Sets a message parameter.
     * @param $value
     * @param $key
     * @return Epr_Com_Message
     */
    public function setValue($value, $key) {
		
		if (is_string($key) || is_numeric($key)) {
		
			if ($value != false && $value != NULL) {
				$this->values[$key] = $value;
			} else {
				if (isset($this->values[$key])) {
					unset($this->values[$key]);
				}
			}
		}
        return $this;
	}

    /**
     * Signs the message with the given key.
     * @param string $sendersPrivateKey
     * @return Epr_Com_Message
     */
    public function sign($sendersPrivateKey) {
        $this->key = $sendersPrivateKey;
		return $this;
	}

    /**
     * Returns true if the authoriy was used to sign the message.
     * @param Epr_Com_Message_Authority $autority
     * @return bool
     */
    public function checkSignatureFromAuthority($autority) {
        if ($this->authority === false || $this->authority === null) {
            if ($autority instanceof Epr_Com_Message_Authority) {
                if ($this->key === $autority->remoteKey()) {
                    $this->authority = $autority;
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * Returns the authority that was used to check the signature.
     * @return Epr_Com_Message_Authority
     */
    public function signatureAuthority() {
        return $this->authority;
    }
	
	public function __toString() {
		
		$document = new DOMDocument('1.0', 'UTF-8');
		
		$xmlRoot = $document->createElement('message');
		$xmlRoot = $document->appendChild($xmlRoot);
		
		$nameElement = $document->createElement('name',$this->getName());
		$xmlRoot->appendChild($nameElement);
		
		if (count($this->values) > 0) {
			
			$valuesElement = $document->createElement('values');
			
			foreach ($this->values as $key => $value) {
				$valueElement = $document->createElement('value',$value);
				
				$keyAttribute = $document->createAttribute('key');
				$keyAttribute->value = $key;
				$valueElement->appendChild($keyAttribute);
				
				
				$valuesElement->appendChild($valueElement);
			}
			
			$xmlRoot->appendChild($valuesElement);
		}

        if ($this->key !== false) {
            $keyElement = $document->createElement('key',$this->key);
            $xmlRoot->appendChild($keyElement);
        }
		
		return $document->saveXML();
	}
	
}

?>