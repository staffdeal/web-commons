<?php


/**
 * Outputs an debug - log
 * @param $message
 */
function debug($message)
{
    _logger()->debug($message);
}

/**
 * echos a styled print_r
 * @param $obj mixed
 */
function debugPrint($obj)
{
    echo "<pre>";
    print_r($obj);
    echo "</pre>";
}


/**
 * Returns the Zend Logger
 * @return Epr_Log
 */
function _logger()
{
    if (Zend_Registry::isRegistered('logger')) {
        return Zend_Registry::get('logger');
    } else {
        echo "Logger unregistred";
    }
}

function _timeSince($timestamp)
{
    return Epr_System::time_since($timestamp);
}

/**
 * Returns the current User
 * @return Epr_User
 */

function _user()
{
    return Epr_System::getUser();
}

/**
 * Returns the API token
 * @return Epr_Token
 */
function _token()
{
	if (Zend_Registry::isRegistered('apiToken')) {
		return Zend_Registry::get('apiToken');
	} else {
		return false;
	}
}

function _tempDir()
{
    return Epr_System::getTempDir();
}


/**
 * Returns the Database-Connection
 * @return Zend_Db_Adapter_Abstract
 */
function _db()
{
    return Epr_System::db();
}

/**
 * Returns the Database-Connection
 * @return Doctrine\ODM\CouchDB\DocumentManager
 */
function _dm()
{
    return Epr_System::dm();
}


/**
 * Returns the Session
 */

function _session()
{
    return Epr_System::getSession();
}

/**
 * Returns the application object.
 * @return Epr_Application
 */
function _app()
{
    return Zend_Registry::get('app');
}

/**
 * Returns the dispatcher's config.
 * @return Zend_Config_Ini
 */
function _dispatcher()
{
	return Zend_Registry::get('dispatcher');
}

/**
 * Returns the mail-Transport
 * @author janfanslau
 * @return Zend_Mail_Transport_Smtp
 */

function _mailTransport()
{
    return Epr_System::getMailTransport();
}

function xmlentities( $string ) { 
	return htmlspecialchars($string, ENT_QUOTES);
}    

function _isDebugging() {
	return (isset($_GET['start_debug']) && $_GET['start_debug'] == '1');
}

/**
 * EPR System Class
 * (c)2012 by Jan Fanslau
 * @version 1.0
 *
 */

class Epr_System
{


    /**
     * Returns the current logged in user
     * @return Epr_User
     */
    static function getUser()
    {

        $auth = Zend_Auth::getInstance()->getIdentity();

        $sesUser = new Zend_Session_Namespace('sesUser');

        if (!isset ($sesUser->user)) {
            $sesUser->user = new Epr_User();
            return $sesUser->user;
        } else {
            if ($sesUser->user instanceof Epr_User) {
                if ($sesUser->user->hasAuth()) {
                    return $sesUser->user;
                } else {
                    $sesUser->user = new Epr_User();
                    return $sesUser->user;
                }
            } else {
                $sesUser->user = new Epr_User();
                return $sesUser->user;
            }
        }
    }

    static function setUser($user)
    {
        $sesUser = new Zend_Session_Namespace('sesUser');
        if ($user instanceof Epr_User) {
            $sesUser->user = $user;
        } else {
            $sesUser->user = new Epr_User('info@gibts.net');
        }
    }


    /**
     * Returns the databaseconnection
     * @return Zend_Db
     */
    static function db()
    {
        return Zend_Registry::get('db');
    }

    /**
     * Returns the databaseconnection
     * @return Doctrine\ODM\CouchDB\DocumentManager
     */
    static function dm()
    {
        return Zend_Registry::get('dm');
    }

    static function dbCache()
    {
        return Zend_Registry::get('dbCache');
    }

    /**
     * returns the Base URL
     * @return String
     */
    static function getBaseURL()
    {
        return Zend_Registry::get('getBaseURL');
    }


    /**
     * Returns the Root Directory
     * @return String
     */
    static function getRootDir()
    {
        return Zend_Registry::get('rootDir');
    }

    /**
     * Returns the Root Directory
     * @return String
     */
    static function getTempDir()
    {
        return Zend_Registry::get('tempDir');
    }


    /**
     * Return random String
     * @return String
     */

    static function getRandomString($lenth = 25)
    {
        // makes a random alpha numeric string of a given lenth
        $aZ09 = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
        $out = '';
        for ($c = 0; $c < $lenth; $c++) {
            $out .= $aZ09[mt_rand(0, count($aZ09) - 1)];
        }
        return $out;
    }

    static function cleanString($string, $quote_style = ENT_NOQUOTES, $charset = false, $double_encode = false)
    {
        $string = (string)$string;

        if (0 === strlen($string)) {
            return '';
        }

        $string = str_replace(" ", "_", $string);

        return $string;
    }

    static function revertURL2Community($string)
    {
        $string = str_replace("_", " ", $string);

        return $string;
    }

    static function getTextForLink($text)
    {
        if (empty($text)) {
            return $text;
        }

        $aTransition = array(
            '$' => '-',
            '@' => 'at',
            '€' => 'e',
            '.' => '-',
            ' ' => '-',
            '_' => '-',
            '¡' => 'i',
            '¢' => '-',
            '£' => '-',
            '¤' => '-',
            '¥' => 'Y',
            '¦' => '-',
            '§' => 'S',
            '¨' => '-',
            '©' => 'C',
            'ª' => 'a',
            '«' => '-',
            '¬' => '-',
            '®' => 'R',
            '¯' => '-',
            '°' => '-',
            '±' => '-',
            '²' => '-',
            '³' => '3',
            '´' => '-',
            'µ' => 'mu',
            '¶' => '-',
            '·' => '-',
            '¸' => '-',
            '¹' => '1',
            'º' => '-',
            '»' => '-',
            '¼' => '1-4',
            '½' => '1-2',
            '¾' => '3-4',
            '¿' => '-',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'AE',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ð' => 'D',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            '×' => 'x',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'P',
            'ß' => 'ss',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            '?*' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            '÷' => '-',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'u',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y'
        );

        $text = strtr($text, $aTransition);

        // restliche Zeichen: weg damit!
        $text = preg_replace("=[^[:alnum:]\-$]=is", '', $text);

        // doppelte -- weg
        $text = preg_replace("=\-{3}=is", '-', $text);
        $text = preg_replace("=\-{2}=is", '-', $text);

        return $text;
    }


    static function getMailTransport()
    {
        return Zend_Registry::get('mailTransport');
    }


    static function getSessionController()
    {
        return _session()->controller;
    }

    static function getSessionAction()
    {
        return _session()->action;
    }

    static function getSessionRequestParams()
    {
        return _session()->params;
    }

    static function getSessionRequestParamsString()
    {
        $retString = "/" . _session()->params['controller'] . '/' . _session()->params['action'];
        if (count($sesSystem->params) > 2) {
            $arr = array_slice($sesSystem->params, 0, -2);

            foreach ($arr as $key => $value) {
                $retString .= "/" . $key . '/' . $value . '/';
            }
        }

        return $retString;
    }

    static function setSessionController($controller)
    {
        $sesSystem = new Zend_Session_Namespace('sesSystem');
        unset($sesSystem->controller);

        $sesSystem->controller = $controller;
    }

    static function setSessionRequestParams($params)
    {
        $sesSystem = new Zend_Session_Namespace('sesSystem');
        unset($sesSystem->params);

        $sesSystem->params = $params;
    }

    static function setSessionAction($action)
    {
        $sesSystem = new Zend_Session_Namespace('sesSystem');
        unset($sesSystem->action);
        $sesSystem->action = $action;

    }


    static function getUniqueId()
    {
        return sprintf('%03d-%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
            Epr_System::ipv4AsArray(4), // 24 bit for the server's ip address
            mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
            mt_rand(0, 65535), // 16 bits for "time_mid"
            mt_rand(0, 4095), // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
            bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
            // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
            // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
            // 8 bits for "clk_seq_low"
            mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"
        );
    }


    static function ipv4AsArray($block = 0)
    {
        $data = explode('.', $_SERVER['SERVER_ADDR']);
        if ($block == 0) {
            return $data;
        } elseif (1 <= $block && $block <= 4) {
            return $data[$block - 1];
        } else return false;
    }

    static function getSession()
    {
        $sesSystem = new Zend_Session_Namespace('sesSystem');
        return $sesSystem;
    }


    // ATOM Zeitstempel umwandeln
    static function time_since($your_timestamp)
    {
        $unix_timestamp = strtotime($your_timestamp);
        $seconds = time() - $unix_timestamp;
        $minutes = 0;
        $hours = 0;
        $days = 0;
        $weeks = 0;
        $months = 0;
        $years = 0;

        if ($seconds == 0) $seconds = 1;
        if ($seconds > 60) {
            $minutes = $seconds / 60;
        } else {
            return Epr_System::germanize_time_since($seconds, 'Sekunde');
        }

        if ($minutes >= 60) {
            $hours = $minutes / 60;
        } else {
            return Epr_System::germanize_time_since($minutes, 'Minute');
        }

        if ($hours >= 24) {
            $days = $hours / 24;
        } else {
            return Epr_System::germanize_time_since($hours, 'Stunde');
        }

        if ($days >= 7) {
            $weeks = $days / 7;
        } else {
            return Epr_System::germanize_time_since($days, 'Tag');
        }

        if ($weeks >= 4) {
            $months = $weeks / 4;
        } else {
            return Epr_System::germanize_time_since($weeks, 'Woche');
        }

        if ($months >= 12) {
            $years = $months / 12;
            return Epr_System::germanize_time_since($years, 'Jahr');
        } else {
            return Epr_System::germanize_time_since($months, 'Monat');
        }
    }

    //Zeitangabe korrigieren
    static function germanize_time_since($num, $word)
    {
        $num = floor($num);
        $addon = "n";
        if ($word == "Tag" || $word == "Monat") $addon = "en";

        if ($num == 1) return "Vor " . $num . " " . $word;
        else return "Vor " . $num . " " . $word . $addon;
    }


    static function http_file_exists($url)
    {
        $f = @fopen($url, "r");
        if ($f) {
            fclose($f);
            return true;
        }
        return false;
    }

    /**
     * Returns the number of weeks in a year
     */
    static function weeksPerYear($year)
    {
        return date("W", mktime(0, 0, 0, 12, 28, $year));
    }

    /**
     * Returns an empty gif image for cookie
     */
    static function getEmptyGif()
    {

        $id = ImageCreate(1, 1);
        $black = ImageColorAllocate($id, 0, 0, 0);
        $white = ImageColorAllocate($id, 255, 255, 255);
        $trans = ImageColorTransparent($id, $white);

        imagefill($id, 0, 0, $white);
        Header("Content-type: image/gif");
        ImageGif($id);
    }

    static function detect_mime($filename)
    {

        if (!is_string($filename)) {
            return false;
        }
        $filetype = strrchr($filename, ".");

        switch ($filetype) {
            case ".zip":
                $mime = "application/zip";
                break;
            case ".ez":
                $mime = "application/andrew-inset";
                break;
            case ".hqx":
                $mime = "application/mac-binhex40";
                break;
            case ".cpt":
                $mime = "application/mac-compactpro";
                break;
            case ".doc":
                $mime = "application/msword";
                break;
            case ".bin":
                $mime = "application/octet-stream";
                break;
            case ".dms":
                $mime = "application/octet-stream";
                break;
            case ".lha":
                $mime = "application/octet-stream";
                break;
            case ".lzh":
                $mime = "application/octet-stream";
                break;
            case ".exe":
                $mime = "application/octet-stream";
                break;
            case ".class":
                $mime = "application/octet-stream";
                break;
            case ".so":
                $mime = "application/octet-stream";
                break;
            case ".dll":
                $mime = "application/octet-stream";
                break;
            case ".oda":
                $mime = "application/oda";
                break;
            case ".pdf":
                $mime = "application/pdf";
                break;
            case ".ai":
                $mime = "application/postscript";
                break;
            case ".eps":
                $mime = "application/postscript";
                break;
            case ".ps":
                $mime = "application/postscript";
                break;
            case ".smi":
                $mime = "application/smil";
                break;
            case ".smil":
                $mime = "application/smil";
                break;
            case ".xls":
                $mime = "application/vnd.ms-excel";
                break;
            case ".ppt":
                $mime = "application/vnd.ms-powerpoint";
                break;
            case ".wbxml":
                $mime = "application/vnd.wap.wbxml";
                break;
            case ".wmlc":
                $mime = "application/vnd.wap.wmlc";
                break;
            case ".wmlsc":
                $mime = "application/vnd.wap.wmlscriptc";
                break;
            case ".bcpio":
                $mime = "application/x-bcpio";
                break;
            case ".vcd":
                $mime = "application/x-cdlink";
                break;
            case ".pgn":
                $mime = "application/x-chess-pgn";
                break;
            case ".cpio":
                $mime = "application/x-cpio";
                break;
            case ".csh":
                $mime = "application/x-csh";
                break;
            case ".dcr":
                $mime = "application/x-director";
                break;
            case ".dir":
                $mime = "application/x-director";
                break;
            case ".dxr":
                $mime = "application/x-director";
                break;
            case ".dvi":
                $mime = "application/x-dvi";
                break;
            case ".spl":
                $mime = "application/x-futuresplash";
                break;
            case ".gtar":
                $mime = "application/x-gtar";
                break;
            case ".hdf":
                $mime = "application/x-hdf";
                break;
            case ".js":
                $mime = "application/x-javascript";
                break;
            case ".skp":
                $mime = "application/x-koan";
                break;
            case ".skd":
                $mime = "application/x-koan";
                break;
            case ".skt":
                $mime = "application/x-koan";
                break;
            case ".skm":
                $mime = "application/x-koan";
                break;
            case ".latex":
                $mime = "application/x-latex";
                break;
            case ".nc":
                $mime = "application/x-netcdf";
                break;
            case ".cdf":
                $mime = "application/x-netcdf";
                break;
            case ".sh":
                $mime = "application/x-sh";
                break;
            case ".shar":
                $mime = "application/x-shar";
                break;
            case ".swf":
                $mime = "application/x-shockwave-flash";
                break;
            case ".sit":
                $mime = "application/x-stuffit";
                break;
            case ".sv4cpio":
                $mime = "application/x-sv4cpio";
                break;
            case ".sv4crc":
                $mime = "application/x-sv4crc";
                break;
            case ".tar":
                $mime = "application/x-tar";
                break;
            case ".tcl":
                $mime = "application/x-tcl";
                break;
            case ".tex":
                $mime = "application/x-tex";
                break;
            case ".texinfo":
                $mime = "application/x-texinfo";
                break;
            case ".texi":
                $mime = "application/x-texinfo";
                break;
            case ".t":
                $mime = "application/x-troff";
                break;
            case ".tr":
                $mime = "application/x-troff";
                break;
            case ".roff":
                $mime = "application/x-troff";
                break;
            case ".man":
                $mime = "application/x-troff-man";
                break;
            case ".me":
                $mime = "application/x-troff-me";
                break;
            case ".ms":
                $mime = "application/x-troff-ms";
                break;
            case ".ustar":
                $mime = "application/x-ustar";
                break;
            case ".src":
                $mime = "application/x-wais-source";
                break;
            case ".xhtml":
                $mime = "application/xhtml+xml";
                break;
            case ".xht":
                $mime = "application/xhtml+xml";
                break;
            case ".zip":
                $mime = "application/zip";
                break;
            case ".au":
                $mime = "audio/basic";
                break;
            case ".snd":
                $mime = "audio/basic";
                break;
            case ".mid":
                $mime = "audio/midi";
                break;
            case ".midi":
                $mime = "audio/midi";
                break;
            case ".kar":
                $mime = "audio/midi";
                break;
            case ".mpga":
                $mime = "audio/mpeg";
                break;
            case ".mp2":
                $mime = "audio/mpeg";
                break;
            case ".mp3":
                $mime = "audio/mpeg";
                break;
            case ".aif":
                $mime = "audio/x-aiff";
                break;
            case ".aiff":
                $mime = "audio/x-aiff";
                break;
            case ".aifc":
                $mime = "audio/x-aiff";
                break;
            case ".m3u":
                $mime = "audio/x-mpegurl";
                break;
            case ".ram":
                $mime = "audio/x-pn-realaudio";
                break;
            case ".rm":
                $mime = "audio/x-pn-realaudio";
                break;
            case ".rpm":
                $mime = "audio/x-pn-realaudio-plugin";
                break;
            case ".ra":
                $mime = "audio/x-realaudio";
                break;
            case ".wav":
                $mime = "audio/x-wav";
                break;
            case ".pdb":
                $mime = "chemical/x-pdb";
                break;
            case ".xyz":
                $mime = "chemical/x-xyz";
                break;
            case ".bmp":
                $mime = "image/bmp";
                break;
            case ".gif":
                $mime = "image/gif";
                break;
            case ".ief":
                $mime = "image/ief";
                break;
            case ".jpeg":
                $mime = "image/jpeg";
                break;
            case ".jpg":
                $mime = "image/jpeg";
                break;
            case ".jpe":
                $mime = "image/jpeg";
                break;
            case ".png":
                $mime = "image/png";
                break;
            case ".tiff":
                $mime = "image/tiff";
                break;
            case ".tif":
                $mime = "image/tiff";
                break;
            case ".djvu":
                $mime = "image/vnd.djvu";
                break;
            case ".djv":
                $mime = "image/vnd.djvu";
                break;
            case ".wbmp":
                $mime = "image/vnd.wap.wbmp";
                break;
            case ".ras":
                $mime = "image/x-cmu-raster";
                break;
            case ".pnm":
                $mime = "image/x-portable-anymap";
                break;
            case ".pbm":
                $mime = "image/x-portable-bitmap";
                break;
            case ".pgm":
                $mime = "image/x-portable-graymap";
                break;
            case ".ppm":
                $mime = "image/x-portable-pixmap";
                break;
            case ".rgb":
                $mime = "image/x-rgb";
                break;
            case ".xbm":
                $mime = "image/x-xbitmap";
                break;
            case ".xpm":
                $mime = "image/x-xpixmap";
                break;
            case ".xwd":
                $mime = "image/x-xwindowdump";
                break;
            case ".igs":
                $mime = "model/iges";
                break;
            case ".iges":
                $mime = "model/iges";
                break;
            case ".msh":
                $mime = "model/mesh";
                break;
            case ".mesh":
                $mime = "model/mesh";
                break;
            case ".silo":
                $mime = "model/mesh";
                break;
            case ".wrl":
                $mime = "model/vrml";
                break;
            case ".vrml":
                $mime = "model/vrml";
                break;
            case ".css":
                $mime = "text/css";
                break;
            case ".html":
                $mime = "text/html";
                break;
            case ".htm":
                $mime = "text/html";
                break;
            case ".asc":
                $mime = "text/plain";
                break;
            case ".txt":
                $mime = "text/plain";
                break;
            case ".rtx":
                $mime = "text/richtext";
                break;
            case ".rtf":
                $mime = "text/rtf";
                break;
            case ".sgml":
                $mime = "text/sgml";
                break;
            case ".sgm":
                $mime = "text/sgml";
                break;
            case ".tsv":
                $mime = "text/tab-separated-values";
                break;
            case ".wml":
                $mime = "text/vnd.wap.wml";
                break;
            case ".wmls":
                $mime = "text/vnd.wap.wmlscript";
                break;
            case ".etx":
                $mime = "text/x-setext";
                break;
            case ".xml":
                $mime = "text/xml";
                break;
            case ".xsl":
                $mime = "text/xml";
                break;
            case ".mpeg":
                $mime = "video/mpeg";
                break;
            case ".mpg":
                $mime = "video/mpeg";
                break;
            case ".mpe":
                $mime = "video/mpeg";
                break;
            case ".qt":
                $mime = "video/quicktime";
                break;
            case ".mov":
                $mime = "video/quicktime";
                break;
            case ".mxu":
                $mime = "video/vnd.mpegurl";
                break;
            case ".avi":
                $mime = "video/x-msvideo";
                break;
            case ".movie":
                $mime = "video/x-sgi-movie";
                break;
            case ".asf":
                $mime = "video/x-ms-asf";
                break;
            case ".asx":
                $mime = "video/x-ms-asf";
                break;
            case ".wm":
                $mime = "video/x-ms-wm";
                break;
            case ".wmv":
                $mime = "video/x-ms-wmv";
                break;
            case ".wvx":
                $mime = "video/x-ms-wvx";
                break;
            case ".ice":
                $mime = "x-conference/x-cooltalk";
                break;
            default:
                $mime = "application/octet-stream";
        }

        _logger()->info('Filename:' . $filename);
        _logger()->info('Filetype:' . $filetype);
        _logger()->info('Mimetype:' . $mime);

        return $mime;
    }

    static function isImage($mime)
    {
        if(strrchr($mime, 'image')){
            return true;
        }
        return false;
    }
}