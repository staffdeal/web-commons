<?php

/** @Document */
class Epr_Module_Setting {

	const TYPE_TEXT = 0;
	const TYPE_INTEGER = 1;
	const TYPE_FLOAT = 2;
	const TYPE_DATE = 3;
	
	/** @Id */
	private $id;
	
	/** Field(type="integer") */
	private $type = -1;
	
	/** Field(type="string") */
	private $value;
	
	/** Field(type="string") */
	private $name;
	
	public function __construct($type,$name,$value = false) {
		if (self::isTypeValid($type)) {
			$this->type = $type;
			$this->name = $name;
			if ($value !== false) {
				$this->setValue($value);
			}
		} else {
			throw new Exception('Type ' . $type . 'not supported by Epr_Module_Setting');
		}
	}
	
	public static function createSetting($type,$name,$value = false) {
		return new self($type,$name,$value);
	}
	
	/**
	 * Creates and returns a setting of type TEXT.
	 * @param string $name
	 * @param string $value
	 * @return Epr_Module_Setting
	 */
	public static function createTextSetting($name,$value = false) {
		return self::createSetting(self::TYPE_TEXT, $name, $value);
	}

	/**
	 * Creates and returns a setting of type INTEGER.
	 * @param string $name
	 * @param integer $value
	 * @return Epr_Module_Setting
	 */
	public static function createIntegerSetting($name, $value) {
		return self::createSetting(self::TYPE_INTEGER, $name, $value);
	}

	/**
	 * Creates and returns a setting of type FLOAT.
	 * @param string $name
	 * @param float $value
	 * @return Epr_Module_Setting
	 */
	public static function createFloatSetting($name, $value) {
		return self::createSetting(self::TYPE_FLOAT, $name, $value);
	}
	
	/**
	 * Creates and returns a setting of type DATE.
	 * @param string $name
	 * @param datetime $value
	 * @return Epr_Module_Setting
	 */
	public static function createDateSetting($name, $value) {
		return self::createSetting(self::TYPE_DATE, $name, $value);
	}
	
	private static function isTypeValid($type) {
		$return = false;
		switch ($type) {
			case self::TYPE_TEXT:
			case self::TYPE_INTEGER:
			case self::TYPE_FLOAT:
			case self::TYPE_DATE:
				$return = true;			
		}
		return $return;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Returns the value of the setting.
	 * @return mixed
	 */	public function getValue() {
		return $this->value;
	}
	
	/**
	 * Sets the value of the setting.
	 * @param mixed $value
	 * @return Epr_Module_Setting
	 * @throws Exception
	 */
	public function setValue($value) {
		if ($this->type < 0) {
			throw new Exception('Cannot assign value until type is set.');
		} else {
			if ($this->getType() == self::TYPE_DATE) {
				if ($value instanceof DateTime) {
					$this->value = (string)$value->getTimestamp();
				} else {
					throw new Exception("Assign non DateTime '$value' to a DATE type");
				}
			} else {
				$this->value = (string)$value;
			}
		}
		return $this;
	}
	
	/**
	 * Returns the name of the setting.
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Sets the name of the setting.
	 * @param string $name
	 * @return Epr_Module_Setting
	 */
	public function setName($name) {
		$this->name = (string)name;
		return $this;
	}
	
	public function getTextValue() {
		if ($this->getType() == self::TYPE_DATE) {
			return $this->getDateValue()->format('Y-m-d h:i:s');	
		} else {
			return (string)$this->value;
		}
	}
	
	public function getIntegerValue() {
		if ($this->getType() == self::TYPE_DATE) {
			return $this->getDateValue()->getTimestamp();
		} else {
			return (int)$this->value;
		}
	}
	
	public function getFloatValue() {
		if ($this->getType() == self::TYPE_DATE) {
			return (float)$this->getIntegerValue();
		} else {
			return (float)$this->value;
		}
	}
	
	public function getDateValue() {
		if ($this->getType() == self::TYPE_DATE) {
			return $this->value;
		} else {
			return new DateTime($this->getTextValue());
		}
	}
	
}
