<?php

use Poundation\PClass;

class Epr_Module_Abstract {

	/** @Id(strategy="ASSIGNED") */
	protected $id;
	
	/** @Field (type="string") */
	protected $displayname;
				
	/** @Field(type="string") */
	protected $authlevel;
	
	static public function instance() {
		$classname = get_called_class();
		
		$instance = false;
		if (Zend_Registry::isRegistered($classname)) {
			$instance = Zend_Registry::get($classname);
		} else {
			$instance = new $classname;
			Zend_Registry::set($classname, $instance);
		}
		
		if ($instance === false) {
			throw new Exception('Could not instanciate module ' . $classname);
		} else {
			return $instance;
		}		
	}
	
	static function identifier() {
		return (string)__(get_called_class())->substringFromPositionOfString('_')->removeLeadingCharactersWhenMatching('_');
	}
	
	public function __construct() {
		$this->id = self::identifier();
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function isActive() {
		return true;
	}
	
	public function getSettingsFormFields() {
		if ($this->hasApiSupport()) {
			$displayName = new Zend_Form_Element_Text('displayname');
			$displayName->setLabel('Display Name');
			$displayName->setRequired(false);
			$displayName->setAttrib('class', 'inp-form');
			$displayName->setDecorators(array('Composite'));
			$displayName->setValue($this->getDisplayname());
				
			$authLevel = new Zend_Form_Element_Select('authlevel');
			$authLevel->setLabel('Access Level');
			$authLevel->setMultiOptions(Epr_Roles::allRoles()->nativeArray());
			$authLevel->setAttrib('class','styledselect_form_1');
			$authLevel->setDecorators(array('Composite'));
			$authLevel->setValue($this->getAPIAuthLevel());
			
			return array(
					'displayname' => $displayName,
					'authlevel' => $authLevel);
		}
		
		return null;
	}
	
	/* (non-PHPdoc)
	 * @see Epr_Module::saveSettingsField()
	*/
	public function saveSettingsField($field) {
		$fieldname = $field->getName();
		$value = $field->getValue();
		
		if ($fieldname == 'displayname') {
			$this->setDisplayname($value);
		} else if ($fieldname == 'authlevel') {
			$this->setAPIAuthLevel($value);
		}
	}
	
	/**
	 * Returns the display name of the module.
	 * @return \Poundation\PString
	 */
	public function getDisplayname() {
		$name = __($this->displayname);
		if ($name->length() == 0) {
			$class = PClass::classFromString(get_called_class());
			$name = $class->invokeMethod('title');
		}
		return $name;
	}
	
	public function setDisplayname($displayname) {
		$this->displayname = (string)$displayname;
	}
	
	public function hasApiSupport() {
		$path = $this->getAPIPath();
		if (is_string($path)) {
			if (strlen($path) > 0) {
				return true;
			}
		} else if (is_array($path)) {
			return (count($path) > 0);
		}
		return false;
	}
	
	public function getAPIPath() {
		return false;
	}
	
	public function getAPIAuthLevel() {
		$role = $this->authlevel;
		if (Epr_Roles::isValidRole($role)) {
			return $role;
		} else {
			return Epr_Roles::ROLE_NONE;
		}
	}
	
	public function setAPIAuthLevel($role) {
		if (Epr_Roles::isValidRole($role)) {
			$this->authlevel = $role;
		}
	}
	
	
}

?>