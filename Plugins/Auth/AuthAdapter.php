<?php
require_once 'Zend/Auth/Adapter/DbTable.php';
class Plugins_Auth_AuthAdapter extends Zend_Auth_Adapter_DbTable
{
    public function __construct()
    {
        $registry = Zend_Registry::getInstance();
        parent::__construct($registry->db);

        $this->setTableName('user');
        $this->setIdentityColumn('username');
        $this->setCredentialColumn('passwort');
        $this->setCredentialTreatment('PASSWORD(?)');
    }
}