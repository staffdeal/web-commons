<?php
class Plugins_Auth_AccessControl extends Zend_Controller_Plugin_Abstract
{
	
	private $_auth;

    private $_acl;
    
    private $_noauth = array('admin' => array('controller' => 'auth', 'action' => 'login'),
    						 'api' => array('controller' => 'error', 'action' => 'noauth'));
    
    private $_noauthApi = array('module' => 'admin',
    							'controller' => 'auth',
    							'action' => 'login');
                             
    private $_noacl = array('module' => 'default',
                            'controller' => 'error',
                            'action' => 'error');

    private $_identity;

	public function __construct(Zend_Auth $auth)
    {
        $this->_auth = $auth;
        $this->_acl = Plugins_Auth_Acl::getInstance();
		Zend_Registry::set('Zend_Auth', $auth);
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

    	if ($request->getModuleName() == 'api') {
    		
			$tokenID = $this->getRequest()->getParam('token',false);
			
			if ($tokenID !== false) {
				$token = _dm()->find('Epr_Token', $tokenID);
			}
			
			if ($tokenID !== false && $token == false) {
				// there was a token parameter but we did not find a token
				
				$request->setModuleName('api');
				$request->setControllerName('error');
				$request->setActionName('token');
				return;	
			}
    		
			if (isset($token)) {
				$role = Epr_Roles::ROLE_USER;
				Zend_Registry::set('apiToken', $token->markAsUsed());
			} else {
				$role = Epr_Roles::ROLE_GUEST;
			}
    		
    		
    	} else {
    	
    		if ($this->_auth->hasIdentity() && is_object($this->_auth->getIdentity())) {
    			$role = $this->_auth->getIdentity()->getRole();
        	} else {
            	$role = Epr_Roles::ROLE_GUEST;
        	}
    	}
    	
        $controller = $request->controller;
        $module = $request->module;
        $action = $request->action;


        //go from more specific to less specific
        $moduleLevel = 'mvc:'.$module;
        $controllerLevel = $moduleLevel . '.' . $controller;
        $privelege = $action;


        if ($this->_acl->has($controllerLevel)) {
            $resource = $controllerLevel;
        } else {
            $resource = $moduleLevel;
        }

        try {
			if ($module != 'default') {
                if ($this->_acl->has($resource) && !$this->_acl->isAllowed($role, $resource, $privelege)) {
                    if (!$this->_identity) {
                    	$request->setControllerName($module);
                        $request->setControllerName($this->_noauth[$module]['controller']);
                        $request->setActionName($this->_noauth[$module]['action']);
                        //$request->setParam('authPage', 'login');
                    } else {
                        $request->setModuleName($this->_noacl['module']);
                        $request->setControllerName($this->_noacl['controller']);
                        $request->setActionName($this->_noacl['action']);
                        //$request->setParam('authPage', 'noauth');
                    }
                    //throw new Exception('Access denied. ' . $resource . '::' . $role);
                }
            }

        } catch (Exception $e) {

            // Repoint the request to the default error handler
            $request->setModuleName('default');
            $request->setControllerName('error');
            $request->setActionName('error');


            // Set up the error handler
            $error = new Zend_Controller_Plugin_ErrorHandler();
            $error->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER;
            $error->noAuth = true;
            $error->request = clone($request);
            $error->exception = $e;
            $request->setParam('error_handler', $error);

        }
        
    }
}